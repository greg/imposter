# imposter

The authors in the commit history are _not_ who they appear to be.

All commits were actually made by Greg (@greg) gmyers@gitlab.com
by changing user.email and user.name locally and pushing to GitLab.

:smile:

